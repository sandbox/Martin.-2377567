<?php
/**
 * @file
 * Api file.
 */

/**
 * Implements hook_views_no_results_wrapper_options().
 */
function hook_views_no_results_wrapper_options() {
  $options = array(
    'p' => array(
      'label' => '<p>',
      'attributes' => array(
        'class' => 'views-no-results-text',
      ),
    ),
  );

  return $options;
}
