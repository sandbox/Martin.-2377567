<?php

/**
 * @file
 * Definition of views_handler_area_text.
 */

/**
 * Views area text handler.
 *
 * @ingroup views_area_handlers
 */
class views_handler_no_results_area extends views_handler_area {

  function render($empty = FALSE) {
    return views_no_results_render_no_results_text();
  }

}
